## IMPORTANT

In order for the tests to run on SDK29 you need to have android studio to run on JDK9

- Download JDK9
- Move it to your Java Home
- Go to File->Project Structure->SKD Location-> Select JSK9
- Re-Run the tests