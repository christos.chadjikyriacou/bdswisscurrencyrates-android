package com.christos.chadjikyriacou.bdswisscurrencyrates.app

import android.app.Application
import com.christos.chadjikyriacou.bdswisscurrencyrates.data.constants.PropertiesManager
import com.christos.chadjikyriacou.bdswisscurrencyrates.data.constants.PropertiesManagerImpl
import com.christos.chadjikyriacou.bdswisscurrencyrates.data.repositories.rates.RatesRepository
import com.christos.chadjikyriacou.bdswisscurrencyrates.data.repositories.rates.RatesRepositoryImpl
import com.christos.chadjikyriacou.bdswisscurrencyrates.domain.rates.RatesViewModel
import com.datatheorem.android.trustkit.TrustKit
import com.example.network.NetworkManager
import kotlinx.coroutines.InternalCoroutinesApi
import org.koin.android.ext.koin.androidContext
import org.koin.core.context.startKoin
import org.koin.core.module.Module
import org.koin.dsl.module

@InternalCoroutinesApi
class CurrencyRatesApplication:Application() {
    init {
        shared = this
    }

    companion object {
        lateinit var shared: CurrencyRatesApplication
            private set
    }

    override fun onCreate() {
        super.onCreate()
        configureKoin()
    }

    private fun configureKoin() {

        startKoin {
            androidContext(this@CurrencyRatesApplication)
            modules(viewModels, repositories, singletons)
        }


        try {
            TrustKit.initializeWithNetworkSecurityConfiguration(this@CurrencyRatesApplication)
        } catch (e: Exception) { }


    }

    private val viewModels: Module = module {
        factory { RatesViewModel(get(), get()) }
    }

    private val singletons:Module = module {
        single<PropertiesManager> { PropertiesManagerImpl() }
        single { com.example.network.NetworkManager.shared }
    }

    private val repositories: Module = module {
        single<RatesRepository> {  RatesRepositoryImpl(get(), get()) }
    }
}