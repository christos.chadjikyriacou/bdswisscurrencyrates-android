package com.christos.chadjikyriacou.bdswisscurrencyrates.data.constants

object Endpoints {

    const val RATES = "rates"
}