package com.christos.chadjikyriacou.bdswisscurrencyrates.data.constants

interface PropertiesManager {
    val refreshInterval:Long
    val priceFloatingPointPrecision:Int
}


class PropertiesManagerImpl:PropertiesManager {

    override val refreshInterval: Long
        get() =  10000
    override val priceFloatingPointPrecision: Int
        get() = 4
}
