import com.google.gson.annotations.SerializedName


data class Rate (

	@SerializedName("symbol") val symbol : String,
	@SerializedName("price") val price : Double
) {

	override fun equals(other: Any?) = (other is Rate) && symbol == other.symbol

}