import com.google.gson.annotations.SerializedName

data class RatesObject (

	@SerializedName("rates") val rates : List<Rate>
)