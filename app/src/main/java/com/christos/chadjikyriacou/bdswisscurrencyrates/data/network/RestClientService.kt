package com.example.christosweatherapp.network

import RatesObject
import android.content.Context
import com.christos.chadjikyriacou.bdswisscurrencyrates.data.constants.Endpoints
import com.example.network.NetworkManager
import retrofit2.http.GET

interface RestClientService {

    @GET(Endpoints.RATES)
    suspend fun getRatesObject():RatesObject

    companion object {
        fun create(context:Context) = NetworkManager(context).retrofitService.create(RestClientService::class.java)
    }

}
