package com.christos.chadjikyriacou.bdswisscurrencyrates.data.repositories.rates

import Rate
import android.app.Application
import com.example.christosweatherapp.network.RestClientService
import com.example.network.NetworkManager
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.flow.flowOn


interface RatesRepository {

    suspend fun getRates(): Flow<List<Rate>>
}


class RatesRepositoryImpl(
    private val application: Application,
    private val networkManager: com.example.network.NetworkManager
) : RatesRepository {

    override suspend fun getRates() = flow {
        emit(RestClientService.create(application.applicationContext).getRatesObject().rates)
    }.flowOn(Dispatchers.IO)

}

