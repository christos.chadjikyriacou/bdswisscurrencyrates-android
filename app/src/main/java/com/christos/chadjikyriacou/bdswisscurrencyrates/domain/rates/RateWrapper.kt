package com.christos.chadjikyriacou.bdswisscurrencyrates.domain.rates

import Rate
import com.christos.chadjikyriacou.bdswisscurrencyrates.data.constants.PropertiesManager
import com.example.utils.utils.Diffable
import org.koin.core.KoinComponent
import org.koin.core.inject

data class RateWrapper(
    private val rate:Rate,
    private var previousPrice:Double?
): KoinComponent,Diffable {

    private val properties: PropertiesManager by inject<PropertiesManager>()

    enum class PriceChange {
        EQUAL_OR_HIGHER, LOWER
    }

    val priceString: String by lazy {
        return@lazy price.toString(properties.priceFloatingPointPrecision)
    }

    val price = rate.price
    val symbol = rate.symbol

    val priceChange: PriceChange? by lazy {
        val previousPrice = previousPrice?.let { it } ?: return@lazy null
        return@lazy if (previousPrice <= rate.price) PriceChange.EQUAL_OR_HIGHER else PriceChange.LOWER
    }


    override fun isSame(other: Any) = (other is RateWrapper) && symbol == other.symbol
    override fun isContentSame(other: Any) = (other is RateWrapper) && this == other


}