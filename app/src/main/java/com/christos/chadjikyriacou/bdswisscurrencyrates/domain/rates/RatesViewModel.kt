package com.christos.chadjikyriacou.bdswisscurrencyrates.domain.rates

import Rate
import android.os.Handler
import android.os.Looper
import androidx.lifecycle.*
import com.christos.chadjikyriacou.bdswisscurrencyrates.data.constants.PropertiesManager
import com.christos.chadjikyriacou.bdswisscurrencyrates.data.repositories.rates.RatesRepository
import kotlinx.coroutines.InternalCoroutinesApi
import kotlinx.coroutines.Job
import kotlinx.coroutines.Runnable
import kotlinx.coroutines.flow.*
import kotlinx.coroutines.launch


@InternalCoroutinesApi
class RatesViewModel(
    private val repository: RatesRepository,
    private val properties: PropertiesManager
) : ViewModel(), LifecycleObserver {

    private var job: Job? = null
    private val mainHandler = Handler(Looper.getMainLooper())
    private val _errorLiveData = MutableLiveData<String?>()
    private val _isLoadingLiveData = MutableLiveData<Boolean>()
    private val _ratesLiveData = MutableLiveData<List<RateWrapper>>()
    val errorLiveData: LiveData<String?> = _errorLiveData
    val ratesLiveData: LiveData<List<RateWrapper>> = _ratesLiveData
    val isLoadingLiveData: LiveData<Boolean> = _isLoadingLiveData


    fun getRates() {

        job = viewModelScope.launch {
            repository.getRates()
                .onStart { _isLoadingLiveData.postValue(true) }
                .onCompletion { _isLoadingLiveData.postValue(false) }
                .catch { onGetRatesError(it.message) }
                .map { transformRates(it) }
                .collect { _ratesLiveData.postValue(it) }
        }
    }

    private fun onGetRatesError(message: String?) {
        _errorLiveData.postValue(message)
    }

    private val getRatesRunnable: Runnable by lazy {
        return@lazy object : Runnable {
            override fun run() {
                getRates()
                mainHandler.postDelayed(this, properties.refreshInterval)
            }
        }
    }

    fun startGettingRates() {
        stopGettingRates()
        mainHandler.post(getRatesRunnable)
    }

    fun stopGettingRates() {
        mainHandler.removeCallbacks(getRatesRunnable)
    }

    private fun transformRates(rates: List<Rate>) = rates.map { transformRate(it) }

    private fun transformRate(rate: Rate) =
        RateWrapper(rate, _ratesLiveData.value?.first { it.symbol == rate.symbol }?.price)


    @OnLifecycleEvent(Lifecycle.Event.ON_STOP)
    private fun onLifeCycleStop() {
        stopGettingRates()
    }

    @OnLifecycleEvent(Lifecycle.Event.ON_RESUME)
    private fun onLifeCycleResume() {
        startGettingRates()
    }
}


