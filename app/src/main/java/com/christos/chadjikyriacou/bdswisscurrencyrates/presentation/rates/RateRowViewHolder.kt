package com.christos.chadjikyriacou.bdswisscurrencyrates.presentation.rates

import android.content.Context
import android.graphics.drawable.GradientDrawable
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import com.christos.chadjikyriacou.bdswisscurrencyrates.R
import com.christos.chadjikyriacou.bdswisscurrencyrates.databinding.RateRowBinding
import com.christos.chadjikyriacou.bdswisscurrencyrates.domain.rates.RateWrapper

class RateRowViewHolder(private val binding: RateRowBinding) : RecyclerView.ViewHolder(binding.root){

    constructor(parent: ViewGroup): this(RateRowBinding.inflate(LayoutInflater.from(parent.context), parent, false))

    fun bind(rate: RateWrapper) {

        binding.symbolTextView.text = rate.symbol
        binding.priceTextView.text = rate.priceString

        val context = binding.root.context
        binding.priceTextView.background = GradientDrawable().apply {
            shape = GradientDrawable.RECTANGLE
            cornerRadius = binding.priceTextView.layoutParams.height / 2f
            color = priceChangeColor(context,rate)
        }
    }


    private fun priceChangeColor(context: Context, rate: RateWrapper) = when (rate.priceChange) {
        null -> ContextCompat.getColorStateList(context, R.color.samePrice)
        RateWrapper.PriceChange.EQUAL_OR_HIGHER -> ContextCompat.getColorStateList(context, R.color.greaterOrSamePrice)
        RateWrapper.PriceChange.LOWER -> ContextCompat.getColorStateList(context, R.color.lowerPrice  )
    }


}