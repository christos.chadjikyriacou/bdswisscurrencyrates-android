package com.christos.chadjikyriacou.bdswisscurrencyrates.presentation.rates



import android.content.DialogInterface
import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import com.christos.chadjikyriacou.bdswisscurrencyrates.R
import com.christos.chadjikyriacou.bdswisscurrencyrates.databinding.ActivityMainBinding
import com.christos.chadjikyriacou.bdswisscurrencyrates.domain.rates.RatesViewModel
import kotlinx.coroutines.InternalCoroutinesApi
import org.koin.androidx.viewmodel.ext.android.viewModel


@InternalCoroutinesApi
class RatesActivity : AppCompatActivity() {


    private val viewModel: RatesViewModel by viewModel()
    private val binding:ActivityMainBinding by lazy { return@lazy ActivityMainBinding.inflate(layoutInflater) }
    private val ratesAdapter: RatesAdapter by lazy { return@lazy RatesAdapter() }


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(binding.root)
        lifecycle.addObserver(viewModel)
        viewModel.startGettingRates()
        configureRecycleView()
        configureObservers()
        configureActionBar()
    }


    private  fun configureActionBar() {
        setSupportActionBar(binding.toolbar)
    }

    private  fun configureRecycleView() {
        binding.ratesRecycleView.adapter = ratesAdapter
        binding.ratesRecycleView.layoutManager = LinearLayoutManager(this)
    }


    private fun configureObservers() {
        viewModel.ratesLiveData.observe(this) {
            ratesAdapter.updateRates(it)
        }

        viewModel.errorLiveData.observe(this) {
            this.viewModel.stopGettingRates()
            it?.let { showError(it) }
        }

        viewModel.isLoadingLiveData.observe(this) {
            binding.toolbarProgressBar.visibility = if (it) View.VISIBLE else View.GONE
        }

    }




    private fun showError(message: String) {
        val alertDialog = AlertDialog.Builder(this).apply {
            setMessage(message)
            setPositiveButton(R.string.retry, DialogInterface.OnClickListener { dialog, id ->
                viewModel.startGettingRates()
            })
        }.create()

        alertDialog.show()
    }

}