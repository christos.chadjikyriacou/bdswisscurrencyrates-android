package com.christos.chadjikyriacou.bdswisscurrencyrates.presentation.rates

import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.example.utils.utils.DiffableListCallback
import com.christos.chadjikyriacou.bdswisscurrencyrates.domain.rates.RateWrapper


class RatesAdapter(private var rates: List<RateWrapper> = emptyList()) : RecyclerView.Adapter<RateRowViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) = RateRowViewHolder(parent)


    fun updateRates(rates: List<RateWrapper>) {
        val diffCallback = DiffableListCallback(this.rates, rates)
        val diff = DiffUtil.calculateDiff(diffCallback)
        this.rates = rates
        diff.dispatchUpdatesTo(this)
    }

    override fun onBindViewHolder(holder: RateRowViewHolder, position: Int) {
        val rate = rates.getOrNull(position)?.let { it } ?: return
        holder.bind(rate)
    }


    override fun getItemCount() = rates.size

}