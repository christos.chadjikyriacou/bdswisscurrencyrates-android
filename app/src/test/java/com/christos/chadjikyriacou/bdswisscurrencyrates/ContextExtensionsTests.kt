package com.christos.chadjikyriacou.bdswisscurrencyrates

import android.content.Context
import android.net.ConnectivityManager
import android.net.NetworkCapabilities
import androidx.test.core.app.ApplicationProvider
import com.example.utils.extensions.isOnline
import org.junit.Assert
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.koin.test.AutoCloseKoinTest
import org.robolectric.RobolectricTestRunner
import org.robolectric.Shadows.shadowOf
import org.robolectric.annotation.Config
import org.robolectric.shadows.ShadowConnectivityManager
import org.robolectric.shadows.ShadowNetworkCapabilities


@RunWith(RobolectricTestRunner::class)
@Config(manifest = Config.NONE, minSdk = 23)
class ContextExtensionsTests: AutoCloseKoinTest() {

    private lateinit var context: Context
    private lateinit var connectivityManager:ConnectivityManager
    private lateinit var shadowConnectivityManager:ShadowConnectivityManager
    private lateinit var shadowOfActiveNetworkCapabilities:ShadowNetworkCapabilities
    private lateinit var networkCapabilities:NetworkCapabilities

    @Before
    fun setUp() {
        context = ApplicationProvider.getApplicationContext<Context>()
        connectivityManager = context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        networkCapabilities = ShadowNetworkCapabilities.newInstance()
        shadowConnectivityManager = shadowOf(connectivityManager)
        shadowOfActiveNetworkCapabilities = shadowOf(networkCapabilities)
    }


    @Test
    fun `is Online no Cellular no Wifi no Ethernet should return false`() {

        shadowOfActiveNetworkCapabilities.removeTransportType(NetworkCapabilities.TRANSPORT_CELLULAR)
        shadowOfActiveNetworkCapabilities.removeTransportType(NetworkCapabilities.TRANSPORT_WIFI)
        shadowOfActiveNetworkCapabilities.removeTransportType(NetworkCapabilities.TRANSPORT_ETHERNET)
        shadowConnectivityManager.setNetworkCapabilities(connectivityManager.activeNetwork,networkCapabilities)
        Assert.assertFalse(context.isOnline())
    }

    @Test
    fun `is Online with Cellular no Wifi no Ethernet should return true`() {
        shadowOfActiveNetworkCapabilities.addTransportType(NetworkCapabilities.TRANSPORT_CELLULAR)
        shadowOfActiveNetworkCapabilities.removeTransportType(NetworkCapabilities.TRANSPORT_WIFI)
        shadowOfActiveNetworkCapabilities.removeTransportType(NetworkCapabilities.TRANSPORT_ETHERNET)
        shadowConnectivityManager.setNetworkCapabilities(connectivityManager.activeNetwork,networkCapabilities)
        Assert.assertTrue(context.isOnline())

    }

    @Test
    fun `is Online no Cellular with Wifi no Ethernet should return true`() {
        val  connectivityManager = context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        val networkCapabilities = ShadowNetworkCapabilities.newInstance()
        val shadowConnectivityManager = shadowOf(connectivityManager)
        val shadowOfActiveNetworkCapabilities = shadowOf(networkCapabilities)
        shadowOfActiveNetworkCapabilities.removeTransportType(NetworkCapabilities.TRANSPORT_CELLULAR)
        shadowOfActiveNetworkCapabilities.addTransportType(NetworkCapabilities.TRANSPORT_WIFI)
        shadowOfActiveNetworkCapabilities.removeTransportType(NetworkCapabilities.TRANSPORT_ETHERNET)
        shadowConnectivityManager.setNetworkCapabilities(connectivityManager.activeNetwork,networkCapabilities)
        Assert.assertTrue(context.isOnline())

    }

    @Test
    fun `is Online no Cellular no Wifi with Ethernet should return true`() {
        shadowOfActiveNetworkCapabilities.removeTransportType(NetworkCapabilities.TRANSPORT_CELLULAR)
        shadowOfActiveNetworkCapabilities.removeTransportType(NetworkCapabilities.TRANSPORT_WIFI)
        shadowOfActiveNetworkCapabilities.addTransportType(NetworkCapabilities.TRANSPORT_ETHERNET)
        shadowConnectivityManager.setNetworkCapabilities(connectivityManager.activeNetwork,networkCapabilities)
        Assert.assertTrue(context.isOnline())

    }
}