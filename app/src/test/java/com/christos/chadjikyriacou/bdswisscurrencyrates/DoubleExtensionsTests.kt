package com.christos.chadjikyriacou.bdswisscurrencyrates

import org.junit.Assert
import org.junit.Test

class DoubleExtensionsTests {


    @Test
    fun doubleToString() {

        val testDouble = 1.22324234234

        Assert.assertEquals(testDouble.toString(-1),"")
        Assert.assertEquals(testDouble.toString(0),"1")
        Assert.assertEquals(testDouble.toString(2),"1.22")
        Assert.assertEquals(testDouble.toString(4),"1.2232")
        Assert.assertEquals(testDouble.toString(6),"1.223242")

    }
}