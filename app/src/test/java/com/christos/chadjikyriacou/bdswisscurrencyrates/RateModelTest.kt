package com.christos.chadjikyriacou.bdswisscurrencyrates

import RatesObject
import com.christos.chadjikyriacou.bdswisscurrencyrates.utils.TestResources

import com.google.gson.Gson
import org.junit.Assert
import org.junit.Test
import java.lang.NumberFormatException


class RateModelTest {

    @Test
    fun ratesModelWithCorrectJSON() {
        val rateObject = Gson().fromJson(TestResources.ratesTestCorrectJSON,RatesObject::class.java)

        Assert.assertEquals(rateObject.rates.size,4)
        Assert.assertEquals(rateObject.rates.first().symbol,"EURUSD")
        Assert.assertEquals(rateObject.rates.first().price, 1.099207977518292,0.0)

        Assert.assertEquals(rateObject.rates.last().symbol,"GBPCHF")
        Assert.assertEquals(rateObject.rates.last().price, 1.1755414804422097,0.0)

    }

    @Test(expected = NumberFormatException::class)
    fun ratesModelWithWrongJSON() {
        Gson().fromJson(TestResources.ratesTestWrongJSON,RatesObject::class.java)
    }
}