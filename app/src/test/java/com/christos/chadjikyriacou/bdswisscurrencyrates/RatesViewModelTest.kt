package com.christos.chadjikyriacou.bdswisscurrencyrates


import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.christos.chadjikyriacou.bdswisscurrencyrates.data.constants.PropertiesManager
import com.christos.chadjikyriacou.bdswisscurrencyrates.data.repositories.rates.RatesRepository
import com.christos.chadjikyriacou.bdswisscurrencyrates.domain.rates.RateWrapper
import com.christos.chadjikyriacou.bdswisscurrencyrates.domain.rates.RatesViewModel
import com.christos.chadjikyriacou.bdswisscurrencyrates.utils.TestCoroutineRule
import com.christos.chadjikyriacou.bdswisscurrencyrates.utils.TestResources
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.InternalCoroutinesApi
import kotlinx.coroutines.flow.flow
import org.junit.Assert
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.rules.TestRule
import org.junit.runner.RunWith
import org.koin.test.AutoCloseKoinTest
import org.mockito.Mock
import org.mockito.Mockito.`when`
import org.mockito.Spy
import org.mockito.junit.MockitoJUnit
import org.mockito.junit.MockitoRule
import org.robolectric.RobolectricTestRunner
import org.robolectric.annotation.Config
import org.robolectric.annotation.LooperMode


/*
**IMPORTANT**
In order for the tests to run on SDK29 you need to have android studio to run on JDK9

- Download JDK9
- Move it to your Java Home
- Go to File->Project Structure->SKD Location-> Select JSK9
- Re-Run the tests
 */

@RunWith(RobolectricTestRunner::class)
@Config(manifest = Config.NONE, minSdk = 23)
@InternalCoroutinesApi
@LooperMode(LooperMode.Mode.PAUSED)
@ExperimentalCoroutinesApi
class RatesViewModelTest : AutoCloseKoinTest() {

    private lateinit var viewModel: RatesViewModel

    @get:Rule
    val testInstantTaskExecutorRule: TestRule = InstantTaskExecutorRule()

    @get:Rule
    val testCoroutineRule = TestCoroutineRule()

    @Spy
    private var ratesLiveData: LiveData<List<RateWrapper>> = MutableLiveData()

    @Mock
    private lateinit var repo: RatesRepository

    @Mock
    private lateinit var properties: PropertiesManager

    @Rule
    @JvmField
    val mockitoRule: MockitoRule = MockitoJUnit.rule()

    @Before
    fun setUp() = testCoroutineRule.runBlockingTest {
        `when`(repo.getRates())
            .thenReturn(flow {
                emit(TestResources.mockPreviousRates) })
            .thenReturn(flow {
                emit(TestResources.mockRates)
        })

        `when`(properties.priceFloatingPointPrecision)
            .thenReturn(4)
        `when`(properties.refreshInterval)
            .thenReturn(1)

        viewModel = RatesViewModel(repo, properties)

        ratesLiveData = viewModel.ratesLiveData

        viewModel.getRates()
        viewModel.getRates()
    }

    @Test
    fun `ratesLiveData value should not be null`() {
        Assert.assertNotNull(ratesLiveData.value)
    }

    @Test
    fun `ratesLiveData value should have the same size as the mock rates`() {
        Assert.assertEquals(ratesLiveData.value?.size, TestResources.mockRates.size)
    }

    @Test
    fun `rateViewModel should have same symbol as rate`() {
        Assert.assertEquals(
            ratesLiveData.value?.first()?.symbol,
            TestResources.mockRates.first().symbol
        )
    }

    @Test
    fun `rateViewModel should have same price as rate`() {
        Assert.assertEquals(
            ratesLiveData.value!!.first().price,
            TestResources.mockRates.first().price,
            0.0
        )
    }

    @Test
    fun `rateViewModel priceString should have the floating point precision from properies manager`() {
        Assert.assertEquals(
            ratesLiveData.value!!.first().priceString.split(".").last().length,
            properties.priceFloatingPointPrecision
        )
    }


    @Test
    fun `rateViewModel priceChange should have the correct value`() {
        val rateViewModels = ratesLiveData.value
        Assert.assertEquals(
            rateViewModels?.get(0)?.priceChange,
            RateWrapper.PriceChange.EQUAL_OR_HIGHER
        )
        Assert.assertEquals(rateViewModels?.get(1)?.priceChange, RateWrapper.PriceChange.LOWER)
        Assert.assertEquals(
            rateViewModels?.get(2)?.priceChange,
            RateWrapper.PriceChange.EQUAL_OR_HIGHER
        )
    }
}


