package com.christos.chadjikyriacou.bdswisscurrencyrates.utils

import Rate

object TestResources {

    val ratesTestWrongJSON = """
    {
       "rates":[
          {
             "symbol":"EURUSD",
             "price":"notADouble"
          },
          {
             "symbol":"GBPUSD",
             "price":1.3152476318962112
          },
          {
             "symbol":"EURGBP",
             "price":0.9089926446114674
          },
          {
             "symbol":"GBPCHF",
             "price":1.1997457098823407
          }
       ]
    }
    """

    val ratesTestCorrectJSON = """
    {
       "rates":[
          {
             "symbol":"EURUSD",
             "price":1.099207977518292
          },
          {
             "symbol":"GBPUSD",
             "price":1.3283924351995655
          },
          {
             "symbol":"EURGBP",
             "price":0.8645656815161293
          },
          {
             "symbol":"GBPCHF",
             "price":1.1755414804422097
          }
       ]
    }
    """

    val mockPreviousRates = listOf<Rate>(
        Rate("EURGBR",1.099207977518292),
        Rate("GBPUSD",1.3283924351995655),
        Rate("EURGBP",0.8645656815161293),
        Rate("GBPCHF",1.1755414804422097))

    val mockRates = listOf<Rate>(
        Rate("EURGBR",2.099207977518292),
        Rate("GBPUSD",0.3283924351995655),
        Rate("EURGBP",0.8645656815161293),
        Rate("GBPCHF",1.1755414804422097))

}