package com.example.network

import android.content.Context
import com.example.utils.extensions.isOnline
import kotlinx.coroutines.InternalCoroutinesApi
import okhttp3.Interceptor
import okhttp3.Response
import java.io.IOException

class ConnectivityInterceptor(private val context: Context):Interceptor {

    @InternalCoroutinesApi
    override fun intercept(chain: Interceptor.Chain): Response {
        if (context.isOnline()) {
            return  chain.proceed(chain.request())
        }

        throw NoConnectivityException(context)
    }
}

class NoConnectivityException(private val context: Context) : IOException() {
    override val message: String
        get() = "No Internet"
}