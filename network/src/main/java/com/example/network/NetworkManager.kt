package com.example.network


import android.content.Context
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit

class NetworkManager(private val context:Context) {

    val retrofitService: Retrofit by lazy {
        Retrofit.Builder()
            .client(generateOkHttpClient())
            .addConverterFactory(GsonConverterFactory.create())
            .build()
    }

//    val networkService: RestClientService by lazy {
//        retrofitService.create(RestClientService::class.java)
//    }


    private fun generateOkHttpClient() = OkHttpClient.Builder()
        .readTimeout(60, TimeUnit.SECONDS)
        .writeTimeout(60, TimeUnit.SECONDS)
        .connectTimeout(60, TimeUnit.SECONDS)
        .addInterceptor(generateLoggingInterceptor())
        .addInterceptor(ConnectivityInterceptor(context))
        .build()



    private fun generateLoggingInterceptor(): HttpLoggingInterceptor {
        return HttpLoggingInterceptor().apply {
            level = HttpLoggingInterceptor.Level.BODY
        }
    }


}