package com.example.utils.extensions

import android.content.Context
import android.net.ConnectivityManager
import android.net.NetworkCapabilities


fun Context.isOnline(): Boolean {
    val connectivityManager =
        (getSystemService(Context.CONNECTIVITY_SERVICE) as? ConnectivityManager)?.let { it }
            ?: return false
    val capabilities =
        connectivityManager.getNetworkCapabilities(connectivityManager.activeNetwork)?.let { it }
            ?: return false


    return (capabilities.hasTransport(NetworkCapabilities.TRANSPORT_CELLULAR) ||
            capabilities.hasTransport(NetworkCapabilities.TRANSPORT_WIFI) ||
            capabilities.hasTransport(NetworkCapabilities.TRANSPORT_ETHERNET))

}