package com.example.utils.extensions


fun Double.toString(digits:Int) = if (digits >= 0) "%.${digits}f".format(this) else ""
